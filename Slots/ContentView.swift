//
//  ContentView.swift
//  Slots
//
//  Created by Raios Kristi on 27.07.2021.
//

import SwiftUI

struct ContentView: View {
    
    @State private var credits = 1000
    @State private var slotOne = 1
    @State private var slotTwo = 1
    @State private var slotThree = 1
    
    var body: some View {
        
        VStack(spacing: 20.0) {
            Spacer()
            Text("SwiftUI Slots!")
                .font(.largeTitle)
            Spacer()
            Text("Credits: \(credits)")
            HStack {
                Image("fruit\(slotOne)").resizable()
                    .aspectRatio( contentMode: .fit)
                Image("fruit\(slotTwo)").resizable()
                    .aspectRatio( contentMode: .fit)
                Image("fruit\(slotThree)").resizable()
                    .aspectRatio( contentMode: .fit)
            }
            Spacer()
            Button("Spin") {
                
                // Randomize the numbers
                slotOne = Int.random(in: 1...3)
                slotTwo = Int.random(in: 1...3)
                slotThree = Int.random(in: 1...3)
                
                // Update credits based on match or not
                if slotOne == slotTwo && slotTwo == slotThree {
                    // Match
                    credits += 45
                }
                else {
                    credits -= 5
                }
            }
            .padding()
            .padding([.leading, .trailing], 40)
            .foregroundColor(.white)
            .background(Color(.systemPink))
            .cornerRadius(25)
            .font(.system(size: 18, weight: .bold, design: .default))
            Spacer()
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
