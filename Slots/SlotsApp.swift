//
//  SlotsApp.swift
//  Slots
//
//  Created by Raios Kristi on 27.07.2021.
//

import SwiftUI

@main
struct SlotsApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
